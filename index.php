<?php
  if( isset($_POST['submit']) ) {

    $name = $_POST['name'];
    $email = $_POST['email'];
    $message = $_POST['message'];

    $minMessageLength = 100;

    $maxMessageLength = 2500;


    if(strlen($message) < $minMessageLength) {

      error_log("Message must contain at least 100 characters!");

    } elseif(strlen($message) > $maxMessageLength) {

      error_log("Message must not contain more than 2500 characters!");

    }

  }
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link
      rel="stylesheet"
      href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
      integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
      crossorigin="anonymous"
    />
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
      crossorigin="anonymous"
    />
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css"
    />
    <link rel="stylesheet" href="css/style.css" />

    <title>Jack Taylor | Portfolio</title>
  </head>
  <body>
    <div class="container">
      <header id="main-header">
        <div class="row no-gutters">
          <div class="col-lg-4 col-md-5">
            <img src="https://placeimg.com/755/743/people/sepia" alt="" />
          </div>
          <div class="col-lg-8 col-md-7">
            <div class="d-fleg flex-column">
              <div class="p-5 bg-dark text-white">
                <div
                  class="d-flex flex-row justify-content-between align-items-center"
                >
                  <h1 class="display-4">Jack Taylor</h1>
                  <div class="d-none d-md-block">
                    <a href="https://twitter.com" class="text-white">
                      <i class="fab fa-twitter"></i
                    ></a>
                  </div>
                  <div>
                    <a href="https://facebook.com" class="text-white">
                      <i class="fab fa-facebook"></i
                    ></a>
                  </div>
                  <div>
                    <a href="https://instagram.com" class="text-white">
                      <i class="fab fa-instagram"></i
                    ></a>
                  </div>
                  <div>
                    <a href="https://github.com" class="text-white">
                      <i class="fab fa-github"></i
                    ></a>
                  </div>
                </div>
              </div>

              <div class="p-4 bg-black">
                Software Developer & Photographer
              </div>

              <div>
                <div
                  class="d-flex flex-row text-white align-items-stretch text-center"
                >
                  <div
                    class="port-item p-4 bg-primary"
                    data-toggle="collapse"
                    data-target="#home"
                  >
                    <i class="fas fa-home fa-2x d-block"></i>
                    <span class="d-none d-sm-block">Home</span>
                  </div>
                  <div
                    class="port-item p-4 bg-success"
                    data-toggle="collapse"
                    data-target="#cv"
                  >
                    <i class="fas fa-graduation-cap fa-2x d-block"></i>
                    <span class="d-none d-sm-block">CV/Resume</span>
                  </div>
                  <div
                    class="port-item p-4 bg-warning"
                    data-toggle="collapse"
                    data-target="#work"
                  >
                    <i class="fas fa-folder-open fa-2x d-block"></i>
                    <span class="d-none d-sm-block">Work</span>
                  </div>
                  <div
                    class="port-item p-4 bg-danger"
                    data-toggle="collapse"
                    data-target="#contact"
                  >
                    <i class="fas fa-envelope fa-2x d-block"></i>
                    <span class="d-none d-sm-block">Contact</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>

      <!-- HOME -->
      <div id="home" class="collapse show">
        <div class="card card-body bg-primary text-white py-5">
          <h2>Welcome To My Page</h2>
          <p class="lead">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur,
            repudiandae.
          </p>
        </div>

        <div class="card card-body py-5">
          <h3>My Skills</h3>
          <p>
            Below is a list of the languages and Architectures that I have
            learned throughout my time in development along with how progressed
            I am in the subjects.
          </p>
          <hr />
          <h4>HTML5</h4>
          <div class="progress mb-3">
            <div class="progress-bar bg-success" style="width: 60%"></div>
          </div>
          <hr />
          <h4>CSS3</h4>
          <div class="progress mb-3">
            <div class="progress-bar bg-success" style="width: 55%"></div>
          </div>
          <hr />
          <h4>Bootstrap 4.0</h4>
          <div class="progress mb-3">
            <div class="progress-bar bg-success" style="width: 80%"></div>
          </div>
          <hr />
          <h4>JavaScript / jQuery</h4>
          <div class="progress mb-3">
            <div class="progress-bar bg-success" style="width: 10%"></div>
          </div>
          <hr />
          <h4>Angular / TypeScript</h4>
          <div class="progress mb-3">
            <div class="progress-bar bg-success" style="width: 30%"></div>
          </div>
          <hr />
          <h4>PHP</h4>
          <div class="progress mb-3">
            <div class="progress-bar bg-success" style="width: 5%"></div>
          </div>
        </div>
      </div>

      <!-- CV / RESUME -->
      <div id="cv" class="collapse">
        <div class="card card-body bg-success text-white py-5">
          <h2>My CV</h2>
          <p class="lead">
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Accusantium, minus.
          </p>
        </div>

        <div class="card card-body py-5">
          <h3>Previous Employment</h3>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Nemo, odit
            optio. Minima accusantium vel libero?
          </p>
          <div class="card-deck">
            <div class="card">
              <div class="card-body">
                <h4 class="card-title">Blue Grape Software Ltd.</h4>
                <p class="card-text">
                  Lorem ipsum dolor, sit amet consectetur adipisicing elit. Cum,
                  esse.
                </p>
                <p class="p-2 mp-3 bg-dark text-white">
                  Position: Apprentice Software & Web Developer
                </p>
                <p class="p-2 mp-3 bg-dark text-white">
                  Phone: 01302 315010
                </p>
                <div class="card-footer">
                  <p class="text-muted">
                    From: November 2017 | To: May 2018
                  </p>
                </div>
              </div>
            </div>

            <div class="card">
              <div class="card-body">
                <h4 class="card-title">One Call Insurance Services Ltd.</h4>
                <p class="card-text">
                  Lorem ipsum dolor, sit amet consectetur adipisicing elit. Cum,
                  esse.
                </p>
                <p class="p-2 mp-3 bg-dark text-white">
                  Position: Apprentice Software Developer
                </p>
                <p class="p-2 mp-3 bg-dark text-white">
                  Phone: 01302 554012
                </p>
                <div class="card-footer">
                  <p class="text-muted">
                    From: February 2019 | To: Present
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- PAST WORK -->
      <div id="work" class="collapse">
        <div class="card card-body bg-warning text-white py-5">
          <h2>My Work</h2>
          <p class="lead">
            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Dicta,
            tempora.
          </p>
        </div>

        <div class="card card-body py-5">
          <h3>What Have I Built?</h3>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste
            voluptatibus numquam fugiat aspernatur! Perferendis, vitae.
          </p>
          <div class="row no-gutters">
            <div class="col-md-3">
              <a
                href="https://unsplash.it/1200/768.jpg?image=252"
                data-toggle="lightbox"
              >
                <img
                  src="https://unsplash.it/600.jpg?image=252"
                  alt=""
                  class="img-fluid"
                />
              </a>
            </div>
            <div class="col-md-3">
              <a
                href="https://unsplash.it/1200/768.jpg?image=253"
                data-toggle="lightbox"
              >
                <img
                  src="https://unsplash.it/600.jpg?image=253"
                  alt=""
                  class="img-fluid"
                />
              </a>
            </div>
            <div class="col-md-3">
              <a
                href="https://unsplash.it/1200/768.jpg?image=254"
                data-toggle="lightbox"
              >
                <img
                  src="https://unsplash.it/600.jpg?image=254"
                  alt=""
                  class="img-fluid"
                />
              </a>
            </div>
            <div class="col-md-3">
              <a
                href="https://unsplash.it/1200/768.jpg?image=255"
                data-toggle="lightbox"
              >
                <img
                  src="https://unsplash.it/600.jpg?image=255"
                  alt=""
                  class="img-fluid"
                />
              </a>
            </div>
          </div>
        </div>
      </div>

      <!-- CONTACT -->
      <div id="contact" class="collapse">
        <div class="card card-body bg-danger text-white py-5">
          <h2>Contact</h2>
          <p class="lead">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis
            delectus tenetur aperiam aspernatur omnis deleniti.
          </p>
        </div>
        <div class="card card-body py-5">
          <h3>Get In Touch</h3>
          <p>
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nulla
            facere adipisci pariatur ratione tempore aspernatur!
          </p>
          <form action="index.php" method="post">
            <div class="form-group">
              <div class="input-group input-group-lg">
                <div class="input-group-prepend prepend-min-width">
                  <span class="input-group-text bg-danger text-white">
                    <i class="fas fa-user"></i>
                  </span>
                </div>
                <input
                  type="text"
                  class="form-control bg-dark text-white contact-form"
                  placeholder="Name"
                  name="name"
                />
              </div>
            </div>
            <div class="form-group">
              <div class="input-group input-group-lg">
                <div class="input-group-prepend prepend-min-width">
                  <span class="input-group-text bg-danger text-white">
                    <i class="fas fa-envelope"></i>
                  </span>
                </div>
                <input
                  type="email"
                  class="form-control bg-dark text-white contact-form"
                  placeholder="Email Address"
                  name="email"
                />
              </div>
            </div>
            <div class="form-group">
              <div class="input-group input-group-lg">
                <div class="input-group-prepend prepend-min-width">
                  <span class="input-group-text bg-danger text-white">
                    <i class="fas fa-pencil-alt"></i>
                  </span>
                </div>
                <textarea
                  class="form-control bg-dark text-white contact-form"
                  placeholder="Message"
                  name="message"
                ></textarea>
              </div>
            </div>

            <input
              type="submit"
              value="Submit"
              class="btn btn-danger btn-block btn-lg"
              name="submit"
            />
          </form>
        </div>
      </div>

      <!-- FOOTER -->
      <footer id="main-footer" class="p-5 bg-dark text-white">
        <div class="row">
          <div class="col-md-6">
            <a href="#" class="btn btn-outline-light">
              <i class="fas fa-cloud"></i> Download CV
            </a>
          </div>
        </div>
      </footer>
    </div>

    <script
      src="http://code.jquery.com/jquery-3.3.1.min.js"
      integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
      integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
      integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
      crossorigin="anonymous"
    ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js"></script>

    <script>
      // Hide Previous Panel
      $(".port-item").click(function() {
        $(".collapse").collapse("hide");
      });

      // Ekko Lightbox
      $(document).on("click", '[data-toggle="lightbox"]', function(e) {
        e.preventDefault();
        $(this).ekkoLightbox();
      });
    </script>
  </body>
</html>
